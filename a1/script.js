let btns = document.querySelectorAll('.btn');



const getAnswer = (event) => {
  let mobileNum = document.getElementById('mobile-num').value
  let tenpesos = Number(document.getElementById('tenpesos').innerHTML);
  let fiftypesos = Number(document.getElementById('fiftypesos').innerHTML);
  let hundredpesos = Number(document.getElementById('hundredpesos').innerHTML);
  let balance = Number(document.getElementById('balance').innerHTML);

  let load = Number(event.target.textContent);

  let remainingBalance = balance - load;

  console.log(remainingBalance);

  if (remainingBalance < 0) {
    document.getElementById('balance').innerHTML =
      remainingBalance + load;
    alert('insufficient balance');
  } else {
    document.getElementById('balance').innerHTML = remainingBalance;
  }

  let transaction = document.getElementById('transaction')

  transaction.innerHTML += `${load} loaded to ${mobileNum}`;

};

for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener('click', getAnswer);
}
